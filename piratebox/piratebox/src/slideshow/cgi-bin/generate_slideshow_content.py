#!/usr/bin/python

import sys
sys.stderr = sys.stdout

import os
import mimetypes
from string import Template

import cgi
path = cgi.FieldStorage().value

print "Content-type:text/html\r\n\r\n"


def load_template():
    file = open('../%stemplate.txt' % path, 'r')
    template = file.read()
    file.close()
    return template


def retrieve_images(path):
    path_content = os.listdir(path)
    for root, dirs, files in os.walk(path):
        root = root.replace('../', '')
        for file in files:
            format, enc = mimetypes.guess_type(file)
            if format and 'image' in format:
                yield (root, file)
                #yield os.path.join(root, file)


def generate_html(images):
    content = ''
    for root, image in images:
        content += '<a href="/%s/"><li><img src="%s" alt=""></li></a>' % (root, os.path.join(root, image))
    return content


def write_template(template):
    file = open("../%sslideshow_content.html" % path, "w")
    file.write(template)
    file.close()


images = retrieve_images('../Shared')
template = Template(load_template()).substitute({'content': generate_html(images), 'path': path[:-1]})
write_template(template)

print input
print "<html><body>ok</body></html>"
