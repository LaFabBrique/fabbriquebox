
function refresh_slideshow(FilePath) {
    $.get(FilePath + 'slideshow_content.html', function(data) {
   		$('div#slideshow').html(data);
   	});
}

$(document).ready(function() {
        var scripts = document.getElementsByTagName('script');
        var thisScript = ""
        for(i=0; i < scripts.length; i++){
            if(scripts[i].src.indexOf("slideshow") > -1) {
                thisScript = scripts[i];
            }
        }
        var path = thisScript.src.replace(/\/script\.js$/, '/');
        var loc = window.location.pathname;
        var dir = loc.substring(0, loc.lastIndexOf('/'));
        _path = path.replace(window.location.protocol + "//" + window.location.hostname + dir + '/', '')
        $.ajax({
            url: dir + '/cgi-bin/generate_slideshow_content.py',
            type: 'POST',
            data: _path,
            async: false,
            success: function (data) {
                refresh_slideshow(_path);
            },
            cache: false,
            contentType: false,
            processData: false
        });

}
);
