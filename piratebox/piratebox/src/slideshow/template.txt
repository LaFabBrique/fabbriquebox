<!DOCTYPE html> <html lang="fr"> <head>
  <link rel="stylesheet" href="$path/responsiveslides.css">
  <script src="$path/responsiveslides.min.js"></script>
  <script>
    $$(function () {
      // Slideshow 1
      $$("#slider1").responsiveSlides({
        maxwidth: 800,
        speed: 800
      });
    });
  </script> </head> <body><h2>Galerie</h2>
    <ul class="rslides" id="slider1">
    	$content
    </ul>
</body>
