
	</div>
</div>
<footer id="about">
	<div class="container">
		<p class="to-top"><a href="#header">Back to top</a></p>
		<h2>Licence</h2>
				<p>Sauf mention contraire explicite, tous les contenus de ce dispositif sont mis &agrave; disposition sous les termes de la licence <a href="https://creativecommons.org/licenses/by/2.0/fr/" target="blank_">CC-BY</a> (Creative Commons - Attribution). C'est &agrave; dire que vous &ecirc;tes autoris&eacute; &agrave; partager et adapter ces contenus pour toute utilisation (m&ecirc;me commerciale), &agrave; condition de cr&eacute;diter l'auteur original.<br/>Ce qui implique que tout les fichiers qui seraient d&eacute;pos&eacute;s sur la Fab'BriqueBox sont de fait plac&eacute;s sous cette m&ecirc;me licence (n'h&eacute;sitez pas &agrave; laisser un message lorsque vous postez un fichier afin que celui-ci puisse vous &ecirc;tre cr&eacute;dit&eacute;).</p>

	</div>
</footer>
