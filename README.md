# Fab'BriqueBox
![logo](https://framagit.org/LaFabBrique/fabbriquebox/raw/master/piratebox/piratebox/www/fabbriquebox_logo_small.png)    
_PirateBox du FabLAb **La Fab'Brique** (www.lafabbrique.org - Salies-de-Béarn - 64)_

Dérivée de la [PirateBox](https://piratebox.cc/) de David Darts, la Fab'BriqueBox propose un espace ouvert et anonyme d'échange de fichiers et de messages (chat, forum).

Informations et notes sur le projet: [voir le wiki de La Fab'Brique](http://lafabbrique.org/wiki/projets:fab_briquebox)

----
## Installation de test réalisée sur un laptop (base Debian)
*Fab'BriqueBox_Pi (installation sur un RaspberryPi 3B) => https://framagit.org/LaFabBrique/fabbriquebox_pi*